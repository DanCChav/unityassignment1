﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	private Rigidbody rb; 
	public float speed; 
	private int count; 
	public Text countText; 
	public Text wintText; 


	void Start ()
	{
		rb = GetComponent<Rigidbody> (); 
		count = 0; 
		SetCountText ();
		wintText.text = ""; 

	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical); 

		rb.AddForce (movement * speed); 

	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Pick Up")) {

			other.gameObject.SetActive (false);
			count++; 
			SetCountText ();

		}
	}

	void SetCountText () {
		countText.text = "Count: " + count.ToString ();
		if (count >= 13)
	  	wintText.text = "YOU WIN!";
	}
}
