﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {

	public float outOfboundsY; 
	public Vector3 respawnLocation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (transform.position.y <= outOfboundsY)
			transform.position = respawnLocation;
	}
}
