﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

	public int buttonWidth; 
	public int buttonHeight; 
	private int origin_x; 
	private int origin_y; 
	// Use this for initialization
	private GUIStyle guiStyle = new GUIStyle();

	void Start () {
		buttonWidth = 200; 
		buttonHeight = 50;
		origin_x = Screen.width / 2 - buttonWidth/2; 
		origin_y = Screen.height/2 + buttonHeight; 
	

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI()
	{
		guiStyle.fontSize = 50;
		guiStyle.normal.textColor = Color.white;
		guiStyle.alignment = TextAnchor.UpperCenter;

		GUI.Label (new Rect (Screen.width/2-50, Screen.height/2 - 50, 100, 50), "Assignment 1", guiStyle);

		if (GUI.Button (new Rect (origin_x, origin_y, buttonWidth, buttonHeight), "Roll - A - Ball")) {
			Application.LoadLevel (1);
		}

		if (GUI.Button (new Rect (origin_x, origin_y + buttonHeight + 10, buttonWidth, buttonHeight), "My Game")) {
			Application.LoadLevel (2);
		}

		if (GUI.Button (new Rect (origin_x, origin_y + buttonHeight + 65, buttonWidth, buttonHeight), "Quit")) {
			Application.Quit ();
		}


	}
}
