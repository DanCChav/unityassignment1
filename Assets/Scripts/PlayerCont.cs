﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerCont : MonoBehaviour {

	private Rigidbody rb; 
	public float speed; 
	private int count; 
	public Text countText; 
	public Text winText; 
	public AudioSource sound;


	void Start ()
	{
		rb = GetComponent<Rigidbody> (); 
		count = 0; 
		SetCountText ();
		winText.text = ""; 
		sound = GetComponent<AudioSource> ();


	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical); 

		rb.AddForce (movement * speed); 

	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Pick Up")) {
			sound.Play ();
			other.gameObject.SetActive (false);
			count++; 
			SetCountText ();

		}
	}

	void SetCountText () {

		if (count <= 26) {
			countText.text = "Count: " + count.ToString () + "/27";
		}


	}
}
