﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	private Rigidbody rb; 
	public float speed; 
	private int count; 
	public Text countText; 
	public Text winText; 
	public AudioSource sound;
	Scene myscene;

	void Start ()
	{
		rb = GetComponent<Rigidbody> (); 
		count = 0; 
		SetCountText ();
		winText.text = ""; 
		sound = GetComponent<AudioSource> ();
		myscene = SceneManager.GetActiveScene ();
	
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical); 

		rb.AddForce (movement * speed); 

	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Pick Up")) {
			sound.Play ();
			other.gameObject.SetActive (false);
			count++; 
			SetCountText ();

		}
	}

	void SetCountText () {
		
		if (count <= 13) {
			countText.text = "Count: " + count.ToString () + "/13";
		}

	
	}
}
